import Vue from "vue";
import App from "./App.vue";
import router from "./router/router";
import store from './store'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import { BootstrapVue } from 'bootstrap-vue'
Vue.use(Element)
Vue.config.productionTip = false;
import JsonExcel from 'vue-json-excel'
import XLSX from 'xlsx'
Vue.use(XLSX)
Vue.use(BootstrapVue)
import axios from 'axios'

Vue.prototype.axios = axios;

Vue.component('downloadExcel', JsonExcel)
// This would usually be an AJAX call to the server or a cookie check

  export const app = new Vue({
    el: '#app',
    render: h => h(App),
    router,
    axios,
    store,
  })

