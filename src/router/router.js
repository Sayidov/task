import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login";
import NavBar from "../views/NavBar";
import Task from "../views/Task";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: "/",
            name: "Login",
            component: Login,
        },

        {
            path: "/navbar",
            name: "Navnar",
            component: NavBar,


        },
        {
            path: "/task",
            name: "Task",
            component: Task,


        },


    ]
});


router.beforeEach((to, from, next) => {
    // const loggedIn = !!TokenService.getToken();
    let token = localStorage.getItem('token');
    if (token || to.name == 'Login') {
        next()
    } else {
        next('/')
    }
})

export default router;
