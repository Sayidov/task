import ApiService from "../service/api.service";
import router from "../router/router";

const state = {
    token: null,
    username: ''
}
const getters = {}
const actions = {


    async signinModule({commit}, payload) {
        try {
            console.log('module')
            const responseAuth = await ApiService.login(payload);
            commit('setSigninRes', responseAuth.data);
            return true;
        } catch (e) {
            return false;
        }
    }


}
const mutations = {
    setSigninRes(state, responseAuth) {
        state.token = responseAuth.accessToken
        state.username = responseAuth.username;
        if (responseAuth[1].accessToken) {
            localStorage.setItem('token', responseAuth[1].accessToken)
            localStorage.setItem('username', responseAuth[0].username)
            router.push('/task');
        }
    }
}


export const auth = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};
