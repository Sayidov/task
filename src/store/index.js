import Vue from 'vue'
import Vuex from 'vuex'
import {auth} from "./auth.module";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        barsCollapse: false
    },
    mutations: {
        setBars(state, collapsed) {
            state.barsCollapse = collapsed
        }

    },
    actions: {},
    modules: {
        auth,
    }
})
