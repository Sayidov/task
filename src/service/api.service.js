import axios from 'axios'

const ApiService = {


    login(data) {
        console.log('service')
        return axios.post('http://localhost:9000/api/auth/signin',data)
    },

    getTasks() {
        return axios.get('http://localhost:9000/api/auth/tasks')
    },

    createTask(task){
        return axios.post('http://localhost:9000/api/auth/create-task',task)
    },
    editSubTask(subTask){
        console.log(subTask)
        return axios.post(`http://localhost:9000/api/auth/task-edit/${subTask.id}`,subTask)

    },
    createSubTask(subTask){
        return axios.post('http://localhost:9000/api/auth/create-subTask',subTask)
    }


}
export default ApiService
